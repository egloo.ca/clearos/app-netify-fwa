
Name: app-netify-fwa
Epoch: 1
Version: 2.6.1
Release: 1%{dist}
Summary: Netify Firewall Agent - API
License: LGPLv3
Group: Applications/API
Packager: eGloo
Vendor: eGloo
Source: app-netify-fwa-%{version}.tar.gz
Buildarch: noarch

%description
The Netify Firewall Agent provides hooks into the ClearOS firewall for application and protocol filtering.

%package core
Summary: Netify Firewall Agent - API
Requires: app-base-core
Requires: app-firewall-core >= 1:2.7.5
Requires: app-network-core
Requires: app-netify-core >= 1:2.6.0
Requires: netify-fwa >= 2.92

%description core
The Netify Firewall Agent provides hooks into the ClearOS firewall for application and protocol filtering.

This package provides the core API and libraries.

%prep
%setup -q
%build

%install
mkdir -p -m 755 %{buildroot}/usr/clearos/apps/netify_fwa
cp -r * %{buildroot}/usr/clearos/apps/netify_fwa/

install -d -m 0755 %{buildroot}/var/clearos/netify_fwa
install -d -m 0755 %{buildroot}/var/clearos/netify_fwa/backup
install -D -m 0644 packaging/10-netify-fwa %{buildroot}/etc/clearos/firewall.d/10-netify-fwa
install -D -m 0644 packaging/netify-fwa.php %{buildroot}/var/clearos/base/daemon/netify-fwa.php

%post core
logger -p local6.notice -t installer 'app-netify-fwa-api - installing'

if [ $1 -eq 1 ]; then
    [ -x /usr/clearos/apps/netify_fwa/deploy/install ] && /usr/clearos/apps/netify_fwa/deploy/install
fi

[ -x /usr/clearos/apps/netify_fwa/deploy/upgrade ] && /usr/clearos/apps/netify_fwa/deploy/upgrade

exit 0

%preun core
if [ $1 -eq 0 ]; then
    logger -p local6.notice -t installer 'app-netify-fwa-api - uninstalling'
    [ -x /usr/clearos/apps/netify_fwa/deploy/uninstall ] && /usr/clearos/apps/netify_fwa/deploy/uninstall
fi

exit 0

%files core
%defattr(-,root,root)
%exclude /usr/clearos/apps/netify_fwa/packaging
%exclude /usr/clearos/apps/netify_fwa/unify.json
%dir /usr/clearos/apps/netify_fwa
%dir /var/clearos/netify_fwa
%dir /var/clearos/netify_fwa/backup
/usr/clearos/apps/netify_fwa/deploy
/usr/clearos/apps/netify_fwa/language
/usr/clearos/apps/netify_fwa/libraries
/etc/clearos/firewall.d/10-netify-fwa
/var/clearos/base/daemon/netify-fwa.php
