<?php

$lang['netify_fwa_app_name'] = 'Netify Firewall Agent';
$lang['netify_fwa_app_description'] = 'The Netify Firewall Agent provides hooks into the ClearOS firewall for application and protocol filtering.';
