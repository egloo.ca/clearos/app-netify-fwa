<?php

/**
 * Netify Firewall Agent (FWA) class.
 *
 * @category   apps
 * @package    netify-fwa
 * @subpackage libraries
 * @author     eGloo <team@egloo.ca>
 * @copyright  2016-2018 eGloo
 * @license    http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 * @link       https://www.egloo.ca/products/clearos
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// N A M E S P A C E
///////////////////////////////////////////////////////////////////////////////

namespace clearos\apps\netify_fwa;

///////////////////////////////////////////////////////////////////////////////
// B O O T S T R A P
///////////////////////////////////////////////////////////////////////////////

$bootstrap = getenv('CLEAROS_BOOTSTRAP') ? getenv('CLEAROS_BOOTSTRAP') : '/usr/clearos/framework/shared';
require_once $bootstrap . '/bootstrap.php';

///////////////////////////////////////////////////////////////////////////////
// T R A N S L A T I O N S
///////////////////////////////////////////////////////////////////////////////

clearos_load_language('netify');
clearos_load_language('netify_fwa');

///////////////////////////////////////////////////////////////////////////////
// D E P E N D E N C I E S
///////////////////////////////////////////////////////////////////////////////

// Classes
//--------

use \clearos\apps\base\Daemon as Daemon;
use \clearos\apps\base\File as File;
use \clearos\apps\firewall\Firewall as Firewall;
use \clearos\apps\network\Network_Utils as Network_Utils;

clearos_load_library('base/Daemon');
clearos_load_library('base/File');
clearos_load_library('firewall/Firewall');
clearos_load_library('network/Network_Utils');

// Exceptions
//-----------

use \clearos\apps\base\Validation_Exception as Validation_Exception;

clearos_load_library('base/Validation_Exception');

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * Netify Firewall Agent (FWA) class.
 *
 * @category   apps
 * @package    netify
 * @subpackage libraries
 * @author     eGloo <team@egloo.ca>
 * @copyright  2016-2018 eGloo
 * @license    http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 * @link       https://www.egloo.ca/products/clearos
 */

class Netify_FWA extends Daemon
{
    ///////////////////////////////////////////////////////////////////////////////
    // C O N S T A N T S
    ///////////////////////////////////////////////////////////////////////////////

    const FILE_CONFIG = '/etc/netify-fwa.conf';

    const IPTABLES_TARGET = 'mangle';
    const IPTABLES_PROTOCOL = 'NETIFY_FWA_PROTOCOL';
    const IPTABLES_SERVICE = 'NETIFY_FWA_SERVICE';

    const TYPE_PROTOCOL = 'protocol';
    const TYPE_APPLICATION = 'service';

    const NETWORK_LOCAL_MAC = 'local_mac';
    const NETWORK_LOCAL_IP = 'local_ip';

    ///////////////////////////////////////////////////////////////////////////////
    // V A R I A B L E S
    ///////////////////////////////////////////////////////////////////////////////

    protected $loaded = FALSE;
    protected $config = array();
    protected $protocol_list = array();
    protected $application_list = array();

    ///////////////////////////////////////////////////////////////////////////////
    // M E T H O D S
    ///////////////////////////////////////////////////////////////////////////////

    /**
     * Netify Firewall Agent (FWA) constructor.
     */

    public function __construct()
    {
        clearos_profile(__METHOD__, __LINE__);

        include clearos_app_base('netify') . '/deploy/netify_protocols.php';
        include clearos_app_base('netify') . '/deploy/netify_applications.php';

        $this->protocol_list = $netify_protocols;
        $this->application_list = $netify_applications;

        parent::__construct('netify-fwa');
    }

    ///////////////////////////////////////////////////////////////////////////////
    // V A L I D A T I O N   R O U T I N E S
    ///////////////////////////////////////////////////////////////////////////////

    /**
     * Validates IP address.
     *
     * @param string $ip IP address
     *
     * @return string error message if IP address is invalid
     */

    public function validate_ip($ip)
    {
        clearos_profile(__METHOD__, __LINE__);

        if (! Network_Utils::is_valid_ip($ip))
            return lang('network_ip_invalid');
    }

    ///////////////////////////////////////////////////////////////////////////////
    // P R I V A T E   M E T H O D S
    ///////////////////////////////////////////////////////////////////////////////

    /**
     * Adds IP to whitelist.
     *
     * @param string $type either self::TYPE_PROTOCOL or self::TYPE_APPLICATION
     * @param string $ip IP address
     *
     * @return void
     * @throws Engine_Exception
     */

    public function _add_whitelist_ip($type, $ip)
    {
        clearos_profile(__METHOD__, __LINE__);

        Validation_Exception::is_valid($this->validate_ip($ip));

        $this->_load_config();
        $this->config[$type . '_whitelist']['local_ip'][] = $ip;

        $this->_write_config();
    }

    /**
     * Deletes IP from whitelist.
     *
     * @param string $type either self::TYPE_PROTOCOL or self::TYPE_APPLICATION
     * @param string $ip IP address
     *
     * @return void
     * @throws Engine_Exception
     */

    public function _delete_whitelist_ip($type, $ip)
    {
        clearos_profile(__METHOD__, __LINE__);

        Validation_Exception::is_valid($this->validate_ip($ip));

        $this->_load_config();

        foreach ($this->config[$type . '_whitelist']['local_ip'] as $key => $ip_value) {
            if ($ip_value == $ip) {
                unset($this->config[$type . '_whitelist']['local_ip'][$key]);
                $this->config[$type . '_whitelist']['local_ip'] = array_values($this->config[$type . '_whitelist']['local_ip']);
            }
        }

        $this->_write_config();
    }

    /**
     * Refreshes configuration file.
     *
     * With protocol ID numbers changing, we sometimes have to delete entries
     * via a shell script.  This leaves gaps in the configuration array, e.g.
     *
     * rule[2] = mangle,NETIFY_FWA_PROTOCOL,121,1
     * rule[3] = mangle,NETIFY_FWA_PROTOCOL,34,1
     * rule[5] = mangle,NETIFY_FWA_PROTOCOL,35,1
     * rule[6] = mangle,NETIFY_FWA_PROTOCOL,32,1
     *
     * @return void
     * @throws Engine_Exception
     */

    public function _refresh_config()
    {
        clearos_profile(__METHOD__, __LINE__);

        $this->_load_config();
        $this->_write_config();
    }

    /**
     * Returns internal rule counts.
     *
     * In order to avoid excessive firewall restarts, we keep track of
     * our state to only restart when necessary.  See issue #3 for details:
     * https://github.com/eglooca/app-netify-fwa/issues/3
     */

    protected function _get_internal_state()
    {
        clearos_profile(__METHOD__, __LINE__);

        $protocol_rules = $this->_get_rules(self::TYPE_PROTOCOL);
        $application_rules = $this->_get_rules(self::TYPE_APPLICATION);

        $protocol_count = 0;
        $application_count = 0;

        foreach ($protocol_rules as $rule) {
            if (!empty($rule['state']) &&  $rule['state'])
                $protocol_count++;
        }

        foreach ($application_rules as $rule) {
            if (!empty($rule['state']) &&  $rule['state'])
                $application_count++;
        }

        return array(
            'protocol_count' => $protocol_count,
            'application_count' => $application_count
        );
    }

    /**
     * Returns rules.
     *
     * @param string $type either self::TYPE_PROTOCOL or self::TYPE_APPLICATION
     *
     * @return array rules
     * @throws Engine_Exception
     */

    protected function _get_rules($type)
    {
        clearos_profile(__METHOD__, __LINE__);

        $this->_load_config();

        if ($type === self::TYPE_PROTOCOL) {
            $config_key = 'protocol_rules';
            $rules_reference = $this->protocol_list;
            $rules = $this->protocol_list;
        } else {
            $config_key = 'service_rules';
            $rules_reference = $this->application_list;
            $rules = $this->application_list;
        }

        if (empty($this->config[$config_key]['rule']))
            return $rules;

        foreach ($this->config[$config_key]['rule'] as $priority => $details) {
            $matches = array();
            $rule_parts = preg_split('/,/', $details);
            $id = $rule_parts[2];

            if (empty($rules_reference[$id])) {
                $name = 'Engine ID: ' . $id;
                $category = lang('base_unknown');
                $url = '';
                $legacy_id = '';
            } else {
                $name = $rules_reference[$id]['name'];
                $category = $rules_reference[$id]['category'];
                $url = $rules_reference[$id]['url'];
                $legacy_id = (empty($rules_reference[$id]['legacy_id'])) ? '' : $rules_reference[$id]['legacy_id'];
            }

            $rules[$id] = array(
                'name' => $name,
                'category' => $category,
                'url' => $url,
                'state' => (bool) $rule_parts[3],
                'priority' => $priority,
                'legacy_id' => $legacy_id,
            );
        }

        return $rules;
    }

    /**
     * Returns state of filter.
     *
     * @param string $type either self::TYPE_PROTOCOL or self::TYPE_APPLICATION
     *
     * @return boolean state of filter
     * @throws Engine_Exception
     */

    protected function _get_state($type)
    {
        clearos_profile(__METHOD__, __LINE__);

        $this->_load_config();

        $key = 'disable_' . $type . '_rules';
        $raw_state = (array_key_exists($key, $this->config['nfa'])) ? $this->config['nfa'][$key] : "true";

        $state = (preg_match('/(true|1)/i', $raw_state)) ? FALSE : TRUE;

        return $state;
    }

    /**
     * Returns IP whitelist
     *
     * @param string $type    either self::TYPE_PROTOCOL or self::TYPE_APPLICATION
     * @param string $network either self::TYPE_IP or self::TYPE_MAC
     *
     * @return array whitelist
     * @throws Engine_Exception
     */

    protected function _get_whitelist($type, $network)
    {
        clearos_profile(__METHOD__, __LINE__);

        $this->_load_config();

        $key = $type . '_whitelist';

        if (empty($this->config[$key][$network]))
            return array();
        else
            return $this->config[$key][$network];
    }

    /**
     * Loads configuration.
     *
     * @return void
     * @throws Engine_Exception
     */

    protected function _load_config()
    {
        clearos_profile(__METHOD__, __LINE__);

        if ($this->loaded)
            return;

        $this->config = parse_ini_file(self::FILE_CONFIG, TRUE);

        // Renumber config in case rules are deleted manually or via
        // a script to deprecate protocol IDs
        if (isset($this->config['protocol_rules']) && isset($this->config['protocol_rules']['rule']))
            sort($this->config['protocol_rules']['rule']);

        if (isset($this->config['service_rules']) && isset($this->config['service_rules']['rule']))
            sort($this->config['service_rules']['rule']);

        $this->loaded = TRUE;
    }

    /**
     * Sets rules.
     *
     * @param string $type either self::TYPE_PROTOCOL or self::TYPE_APPLICATION
     * @param array $rules list of rules
     *
     * @return array void
     * @throws Engine_Exception
     */

    protected function _set_rules($type, $rules)
    {
        clearos_profile(__METHOD__, __LINE__);

        $before = $this->_get_internal_state();

        $this->_load_config();

        $this->config[$type . '_rules']['rule'] = array();
        $count = 0;

        foreach ($rules as $id => $state) {
            $state = (preg_match('/on/', $state)) ? 1 : 0;

            if ($type === self::TYPE_PROTOCOL)
                $rule = self::IPTABLES_TARGET . ',' . self::IPTABLES_PROTOCOL . ',' . $id . ',' . $state;
            else
                $rule = self::IPTABLES_TARGET . ',' . self::IPTABLES_SERVICE . ',' . $id . ',' . $state;

            $this->config[$type . '_rules']['rule'][] = $rule;
            $count++;
        }

        $this->_write_config();

        // Restart firewall if state required.  See https://github.com/eglooca/app-netify-fwa/issues/3
        // This is a bit non-standard in our API, but...

        $after = $this->_get_internal_state();
        $restart = FALSE;

        if (($before['protocol_count'] ==  0) && ($after['protocol_count'] >= 1)) {
            clearos_log('netify-fwa', 'Detected protocol on rule transition');
            $restart = TRUE;
        } else if (($before['protocol_count'] >= 1) && ($after['protocol_count'] == 0)) {
            clearos_log('netify-fwa', 'Detected protocol off rule transition');
            $restart = TRUE;
        } else if (($before['application_count'] == 0) && ($after['application_count'] >= 1)) {
            clearos_log('netify-fwa', 'Detected application on rule transition');
            $restart = TRUE;
        } else if (($before['application_count'] >= 1) && ($after['application_count'] == 0)) {
            clearos_log('netify-fwa', 'Detected application off rule transition');
            $restart = TRUE;
        }

        if ($restart) {
            $firewall = new Firewall();
            $firewall->restart();
        }
    }

    /**
     * Sets state of filter.
     *
     * @param string  $type either self::TYPE_PROTOCOL or self::TYPE_APPLICATION
     * @param boolean $state state of filter
     *
     * @return void
     * @throws Engine_Exception
     */

    protected function _set_state($type, $state)
    {
        clearos_profile(__METHOD__, __LINE__);

        $this->_load_config();

        $key = 'disable_' . $type . '_rules';
        $value = ($state) ? FALSE : TRUE;

        $this->config['nfa'][$key] = $value;

        $this->_write_config();
    }

    /**
     * Update state.
     *
     * Since the Netify FWA is used by two apps, we have a special method
     * to stop, start, and reload the netify-fwa daemon.  Any time a filter
     * configuration is updated (e.g. adding BitTorrent blocking) or the
     * state changes (e.g. Protocol filter is disabled), the caller should
     * run this method to update the state.
     *
     * @return void
     * @throws Engine_Exception
     */

    protected function _update_state()
    {
        clearos_profile(__METHOD__, __LINE__);

        $this->_load_config();
        
        $service_filter_enabled = (preg_match('/(true|1)/', $this->config['nfa']['disable_service_rules'])) ? FALSE : TRUE;
        $protocol_filter_enabled = (preg_match('/(true|1)/', $this->config['nfa']['disable_protocol_rules'])) ? FALSE : TRUE;
        $fwa_running = $this->get_running_state();

        if ($fwa_running) {
            if ($service_filter_enabled || $protocol_filter_enabled) {
                $this->reset(TRUE);
            } else {
                $this->set_running_state(FALSE);
                $this->set_boot_state(FALSE);
            }
        } else {
            if ($service_filter_enabled || $protocol_filter_enabled) {
                $this->set_running_state(TRUE);
                $this->set_boot_state(TRUE);
            }
        }
    }

    /**
     * Writes configuration file.
     *
     * @return void
     * @throws Engine_Exception
     */

    protected function _write_config()
    {
        clearos_profile(__METHOD__, __LINE__);

        // TODO: discuss priorities.  Rules are just enumerated right now.

        $sections_with_priorities = array(
            'service_whitelist',
            'protocol_whitelist',
            'service_rules',
            'protocol_rules'
        );

        $keys_with_booleans = array(
            'disable_protocol_rules',
            'disable_service_rules'
        );

        $contents = '';

        foreach ($this->config as $section => $subsection) {
            $contents .= "[$section]\n";

            foreach ($subsection as $key => $value) {
                for ($inx = 0; $inx < count($value); $inx++) {
                    if (in_array($section, $sections_with_priorities)) {
                        $contents .= $key . '[' . $inx . '] = ' . $value[$inx] . "\n";
                    } else {
                        if (in_array($key, $keys_with_booleans))
                            $value = ($value) ? 'true' : 'false';

                        $contents .= $key . ' = '  . $value . "\n";
                    }
                }
            }

            $contents .= "\n";
        }

        $filename = self::FILE_CONFIG;
        $file = new File($filename . '.clearos');

        if ($file->exists())
            $file->delete();

        $file->create('root', 'root', '0644');
        $file->add_lines(trim($contents) . "\n");
        $file->move_to($filename);

        $this->loaded = FALSE;
    }
}
