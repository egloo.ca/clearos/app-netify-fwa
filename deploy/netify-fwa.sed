# Netify FWA rule parser for firewall scriptlet

# Remove rule prefix
s/^rule\[[0-9]*\][[:space:]]*=[[:space:]]*//g

# Remove rule enabled flag from end of rule
s/,1$//g
s/,true$//g

# Substitute commas with spaces
s/,/ /g

