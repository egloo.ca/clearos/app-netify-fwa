<?php

/////////////////////////////////////////////////////////////////////////////
// General information
/////////////////////////////////////////////////////////////////////////////

$app['basename'] = 'netify_fwa';
$app['version'] = '2.6.1';
$app['vendor'] = 'eGloo';
$app['packager'] = 'eGloo';
$app['license'] = 'GPLv3';
$app['license_core'] = 'LGPLv3';
$app['description'] = lang('netify_fwa_app_description');

/////////////////////////////////////////////////////////////////////////////
// App name and categories
/////////////////////////////////////////////////////////////////////////////

$app['name'] = lang('netify_fwa_app_name');
$app['category'] = lang('base_category_gateway');
$app['subcategory'] = 'Filtering';

/////////////////////////////////////////////////////////////////////////////
// Controllers
/////////////////////////////////////////////////////////////////////////////

$app['controllers']['netify']['title'] = $app['name'];
$app['controllers']['settings']['title'] = lang('base_settings');

/////////////////////////////////////////////////////////////////////////////
// Packaging
/////////////////////////////////////////////////////////////////////////////

$app['core_only'] = TRUE;

$app['core_requires'] = array(
    'app-firewall-core >= 1:2.7.5',
    'app-network-core',
    'app-netify-core >= 1:2.6.0',
    'netify-fwa >= 2.92',
);

$app['core_file_manifest'] = array(
    'netify-fwa.php'=> array('target' => '/var/clearos/base/daemon/netify-fwa.php'),
    '10-netify-fwa' => array(
        'target' => '/etc/clearos/firewall.d/10-netify-fwa',
        'mode' => '0644',
        'owner' => 'root',
        'group' => 'root',
    ),
);

$app['core_directory_manifest'] = array(
   '/var/clearos/netify_fwa' => array(),
   '/var/clearos/netify_fwa/backup' => array(),
);
